﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Requests;

/// <summary>
/// Информация для запроса на обновление работника
/// </summary>
public class UpdateEmployeeRequest
{
    /// <summary>
    /// Имя
    /// </summary>
    public string FirstName { get; set; }
    
    /// <summary>
    /// Фамилия
    /// </summary>
    public string LastName { get; set; }
    
    /// <summary>
    /// Электронная почта
    /// </summary>
    public string Email { get; set; }
}