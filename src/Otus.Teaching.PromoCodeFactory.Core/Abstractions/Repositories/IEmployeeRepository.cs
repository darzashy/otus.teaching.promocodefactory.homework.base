﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Requests;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

/// <summary>
/// Интерфейс управления работниками
/// </summary>
public interface IEmployeeRepository
{
    Task<IEnumerable<Employee>> GetAllAsync();
        
    Task<Employee> GetByIdAsync(Guid id);

    Task<Guid> CreateAsync(CreateEmployeeRequest request);

    Task UpdateAsync(Guid employeeId, UpdateEmployeeRequest request);
    
    Task DeleteAsync(Guid employeeId);
}