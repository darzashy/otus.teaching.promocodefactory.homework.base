﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Requests;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EmployeeRepository : IEmployeeRepository
{
    private IEnumerable<Employee> Data { get; set; }
    
    public EmployeeRepository(IEnumerable<Employee> data)
    {
        Data = data;
    }
    
    public async Task<IEnumerable<Employee>> GetAllAsync()
    {
        return await Task.FromResult(Data);
    }

    public async Task<Employee> GetByIdAsync(Guid id)
    {
        return await Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
    }

    public async Task<Guid> CreateAsync(CreateEmployeeRequest request)
    {
        if (Data.Any(x => x.Email.ToLower() == request.Email.ToLower()))
        {
            throw new HttpRequestException("Пользователь с такой почтой уже существует");
        }

        var id = Guid.NewGuid();
        Data = Data.Append(new Employee
        {
            Id = id,
            FirstName = request.FirstName,
            LastName = request.LastName,
            Email = request.Email,
            AppliedPromocodesCount = 0
        });

        return await Task.FromResult(id);
    }

    public async Task UpdateAsync(Guid employeeId, UpdateEmployeeRequest request)
    {
        if (Data.Any(x => x.Email.ToLower() == request.Email.ToLower() && x.Id != employeeId))
        {
            throw new HttpRequestException("Пользователь с такой почтой уже существует");
        }

        var employee = Data.FirstOrDefault(x => x.Id == employeeId);
        if (employee == null)
        {
            throw new HttpRequestException("Пользователь не найден");
        }

        employee.FirstName = request.FirstName;
        employee.LastName = request.LastName;
        employee.Email = request.Email;
    }

    public async Task DeleteAsync(Guid employeeId)
    {
        Data = Data.Where(x => x.Id != employeeId);
    }
}