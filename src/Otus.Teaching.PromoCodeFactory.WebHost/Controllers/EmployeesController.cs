﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Requests;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeesController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создание сотрудника
        /// </summary>
        /// <param name="request">Информация для запроса</param>
        /// <returns>Идентификатор созданного сотрудника</returns>
        [HttpPost]
        [Produces(typeof(Guid))]
        public async Task<ActionResult<Guid>> CreateEmployee(CreateEmployeeRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            
            var id = await _employeeRepository.CreateAsync(request);
            return id;
        }

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        /// <param name="request">Информация  для запроса</param>
        /// <exception cref="ArgumentNullException"></exception>
        [HttpPut("{employeeId:guid}")]
        public async Task UpdateEmployee(Guid employeeId, UpdateEmployeeRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            
            await _employeeRepository.UpdateAsync(employeeId, request);
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="employeeId">Идентификатор сотрудника</param>
        [HttpDelete("{employeeId:guid}")]
        public async Task DeleteAsync(Guid employeeId)
        {
            await _employeeRepository.DeleteAsync(employeeId);
        }
    }
}